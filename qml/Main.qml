import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3 as Ubuntu
import Morph.Web 0.1
import QtSystemInfo 5.5

ApplicationWindow {
    id: root
    visible: true
    color: colors.background

    width: units.gu(45)
    height: units.gu(75)

    property QtObject colors: QtObject {
        readonly property color divider: '#1A1A1A'
        readonly property color text: '#52B54B'
        readonly property color background: '#1F1F1F'
    }

    Settings {
        id: settings

        property string url
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Android 8.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.151 Mobile Safari/537.36 Ubuntu Touch Webapp'
        offTheRecord: false
    }

    WebView {
        id: webview
        anchors.fill: parent

        // TODO prevent navigation away from this domain
        // TODO error page
        context: webcontext
        visible: url

        onFullScreenRequested: function(request) {
            if (request.toggleOn) {
                root.showFullScreen();
            }
            else {
                root.showNormal();
            }
            request.accept();
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;


            var domain = settings.url.replace('http://', '').replace('https://', '');
            if (domain.indexOf('/') >= 0) {
                domain = domain.substring(0, domain.indexOf('/'));
            }

            if (!url.match('(http|https)://' + domain + '/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }

        Component.onCompleted: {
            if (settings.url.length > 0) {
                url = settings.url;
            }
            else {
                bottomEdge.commit();
            }
        }
    }

    Label {
        anchors.centerIn: parent
        visible: !webview.url

        text: i18n.tr('Swipe up from the bottom to set your Emby server\'s url')
        horizontalAlignment: Label.AlignHCenter
    }

    Ubuntu.BottomEdge {
        id: bottomEdge

        hint.text: i18n.tr('Settings')
        hint.visible: !webview.isFullScreen
        preloadContent: true

        height: parent.height
        width: parent.width

        contentComponent: Page{
            width: bottomEdge.width
            height: bottomEdge.height

            header: ToolBar {
                id: bottomEdgeHeader
                height: units.gu(6)

                RowLayout {
                    anchors {
                        fill: parent
                        leftMargin: units.gu(1)
                        rightMargin: units.gu(1)
                    }

                    ToolButton {
                        id: backButton

                        Layout.fillHeight: true
                        Layout.preferredWidth: height
                        Layout.topMargin: units.gu(1.5)
                        Layout.bottomMargin: units.gu(1.5)

                        contentItem: Ubuntu.Icon {
                            name: 'down'
                            color: colors.text
                        }

                        onClicked: bottomEdge.collapse()

                        // Don't show the pressed background
                        background: Rectangle { visible: false }
                    }

                    Label {
                        Layout.fillWidth: true
                        color: colors.text
                        text: i18n.tr('Settings')

                        font.pixelSize: units.gu(3)
                        verticalAlignment: Qt.AlignVCenter
                    }
                }

                background: Rectangle {
                    implicitHeight: units.gu(6)
                    color: colors.background

                    Rectangle {
                        anchors {
                            left: parent.left
                            right: parent.right
                            top: parent.bottom
                        }

                        height: units.dp(1)
                        color: colors.divider
                    }
                }
            }


            Flickable {
                anchors.fill: parent

                clip: true
                contentHeight: settingsList.height + units.gu(4)

                ColumnLayout {
                    id: settingsList
                    anchors {
                        top: parent.top
                        right: parent.right
                        left: parent.left
                        margins: units.gu(1)
                    }

                    spacing: units.gu(1)

                    Label {
                        text: i18n.tr('Emby server url (ex: http://192.168.1.2:8096)')
                    }

                    TextField {
                        id: urlSetting
                        Layout.fillWidth: true

                        inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

                        onAccepted: {
                            focus = false;
                            bottomEdge.collapse();
                        }
                        onTextChanged: {
                            var url = text;
                            if (url.lenght > 0 && url.indexOf('https://') !== 0 && url.indexOf('http://') !== 0) {
                                url = 'http://' + url;
                            }

                            settings.url = url;
                        }

                        Component.onCompleted: text = settings.url
                    }
                }
            }
        }

        onCollapseStarted: webview.url = settings.url
    }

    ScreenSaver {
        screenSaverEnabled: !Qt.application.active || !webview.recentlyAudible
    }
}
